---
marp: true
theme: uncover
footer: 'NetDevOps Days - NYC 2023-10'

---

## **Test it first! Taking a DevOps approach to network labs**

Pete Crocker
[pete.crocker@ipfabric.io](mailto:pete.crocker@ipfabric.io)
Jerrod Gerth
[jerrod.gerth@nokia.com](mailto:jerrod.gerth@nokia.com)

![bg](bg3.png)

---

## Marp - Slides as Code

`https://gitlab.com/petercrocker/netdevops-days-2023-10`

![bg](bg3.png)

---

## Audience Participation

<!-- First, let’s see where network automation is at. So we can understand who’s in the room today:
* Who’s a network engineer?
* Who’s a developer?
* Zero, One hand raised, or two hands? How much automation are you doing now? None, basics, advanced?
* Who’s doing this in your company from the grassroots? As in you started the initiative yourself?
* Who’s got management buy-in?
  * Really? Who gets time to step away from firefighting?
* Who’s doing artisanal coding?
  * Using frameworks, like ansible?
  * Using commercial tools? 
-->

![bg](bg3.png)

---

## Backstory

* Network engineer @ large SP in the mid-90s
* 2014 - DevOps sink or swim

<!-- My first professional love! Sun Sparc 20, Livingston Portmaster & dialup modems. -->

<!-- Really got in to the dev and automation world in 2014 when I was at a startup and was surrounded by 100% hardcore developers doing hardcore modern development. Everything CI-CD, full test coverage. -->

![bg right:35%](sun.jpeg)

---

## What's the Problem?

* Change Faster
* De-Risk Change
<!-- You've got two competing priorities. -->

![bg](bg3.png)

---

## One Way to De-Risk Change

<!-- A MoP gives you CYA, but it's history of past mistakes
not future mistakes. -->

![bg right:60%](mop.jpg)

---

## Simulation vs. Emulation

<!-- 
Simulation: Creates an abstract model of the network

Emulation: Spin up vendor VMs. NOTE: Vendor VMs are different from reality!
-->

![bg](bg3.png)

---

## Simulators

![WANDL](wandl.webp)![Cariden](cariden.webp)
![width:400px](ipf.png)
![Forward Networks](forward.png) ![width:180px](batfish.png)



![bg](bg3.png)

---

#### Pros

* Detailed model of network state <!-- Vary from simplified control plane to L2/L3/L4 modeling of device behavior -->
* Scales well <!-- 10,000+ devices -->
* Some can model L2 & L4 behavior <!-- End to end WiFi to Campus across WAN to DC VxLAN to Cloud -->

#### Cons

* Not Full Network Behavior Model <!-- Vendor-abstracted model -->
* Device Support <!-- Each simulator must support each NOS -->

![bg](bg3.png)

---

## Emulators

![width:300px](dynamips.png) ![width:150px](arrow.jpg) ![EVE](eve.png) ![width:150px](arrow.jpg)
<!-- But we want to run our lab as code! -->
![width:300px](vagrant.png) ![ContainerLab](containerlab.png) ![width:300px](netlab.png)
<!-- NetLab is an orchestrator of Vagrant and/or ContainerLab -->
![bg](bg3.png)

---

#### Pros

* Config changes can be tested <!-- Including failure cases. Chaos monkey example -->
* Test your automation! <!-- Does your ordering of steps work? -->
* Can potentially cover L2 & L4 behavior <!-- Buyer beware of vendor VM limitations! -->

#### Cons

* Getting VMs for all your vendors <!-- Not all vendors have VMs -->
* Lack of proper L2 behavior in VMs <!-- Example: Different IOS image for L2 behavior because it's in the ASIC, not control plane. -->
* Cost! <!-- Choose your vendors carefully! Also code maintenance. -->
* Requirements of vendor VMs <!-- CPU/Mem -->
* Interface limits and naming in VMs <!-- 8 interfaces, Eth1 vs. GigE0/0 -->

![bg](bg3.png)

---

## Summary

* Modeling Configs to Simulate Current State <!-- Either SP-specific tools, or limited vendor support from Batfish -->
* Learning State and Creating a Simulation <!-- Can't do pre-change simulation. -->
* Creating a One-To-One Network Emulation <!-- Good for testing automation, but bad at scale and vendor support. -->

![bg](bg3.png)
